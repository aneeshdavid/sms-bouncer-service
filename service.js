var repo = require('./repository');
const UIDGenerator = require('uid-generator');
const uidgen = new UIDGenerator();

exports.add = async function (userObj) {
    const uuid = await uidgen.generate();
    userObj.apiKey = uuid;
    return repo.save(userObj);
}

