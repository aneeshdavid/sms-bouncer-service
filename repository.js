
var UserModel = require('./models/userModel');

exports.save = async function (userObj) {
    var userModel = new UserModel(userObj);
    const data = await userModel.save(userObj);
    return data;
}
