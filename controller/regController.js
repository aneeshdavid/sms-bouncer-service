var express = require('express');
var route = express.Router();

var service = require('../service');

route.post("/", async function (req, res) {
    const result = await service.add(req.body);
    res.send(result.apiKey);
});

module.exports.router = route;


