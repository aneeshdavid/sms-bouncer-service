const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema({
    userId: {
        type: String,
        require: true
    },
    apiKey: {
        type: String,
        require: true,
    },
    fireBasetoken: {
        type: String,
        require: true
    },
    mapped_to: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'userSchema'
    }]

}, {
        timestamps: true
    });

module.exports = mongoose.model('user_info', userSchema, 'user_info');
