const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const deviceSchema = new Schema({
    userId: {
        type: String,
        require: true
    },
    message: {
        type: String,
        require: true
    }
}, {
        timestamps: true
    });

module.exports = mongoose.model('sms_info', deviceSchema, 'sms_info');
