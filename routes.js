var express = require('express');
var router = express.Router();


var tokenValidator = function (req, res, next) {
    var accessToken = req.headers['access-token'];
    //Check for access token
    if (accessToken && validateToken(accessToken)) {
        return next();
    }
    // Check for accessKey
    var accesskey = req.headers['access-key'];
    if (accesskey && validateAccessKey(accesskey)) {
        var accessToken = generateToken(accesskey);
        res.headers.accessToken = accessToken;
        return next();
    } else {
        //"access-key-not-found" : 1000
        //"invalid-access-key" : 1001
        var errCode = accesskey ? 1001 : 1000
        return res.status(401).send({
            ERROR_CODE: errCode
        });
    }
};

var validateToken = function () {
    return true;
};
var validateAccessKey = function () {
    return true;
};
var generateToken = function () {
    return "#token";
};





router.use('/register', tokenValidator, require('./controller/regController').router);
router.use('/sms', tokenValidator, require('./controller/smsController').router);
module.exports.router = router;